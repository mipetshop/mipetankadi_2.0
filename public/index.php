<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../src/config/DbConnection.php';
require '../src/util/CommonUtil.php';

$app = new \Slim\App;
// Route to all GET request
require '../src/routes/GetRequest.php';
// Route to all POST request
require '../src/routes/PostRequest.php';
// Route to all PUT request
require '../src/routes/PutRequest.php';
$app->run();