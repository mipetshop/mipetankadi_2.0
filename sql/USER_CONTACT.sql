CREATE TABLE USER_CONTACT(
   CONTACT_ID   INT  NOT NULL AUTO_INCREMENT,
   CONTACT_NAME VARCHAR (100),
   MOBILE_NO VARCHAR (20),
   ADDRESS_ONE VARCHAR (100),
   ADDRESS_TWO VARCHAR (100),
   CITY VARCHAR (100),
   DISTRICT VARCHAR (100),
   STATE VARCHAR (100),
   COUNTRY VARCHAR (100),
   ZIP_CODE VARCHAR (50),
   CREATED_ON   DATETIME, 
   MODIFIED_ON   DATETIME, 
   CREATED_BY    INT,
   MODIFIED_BY INT,
   STATUS VARCHAR (10),
   IS_PRIMARY_CONTACT VARCHAR (10),
   USER_ID INT,
   PRIMARY KEY (CONTACT_ID),
   FOREIGN KEY (USER_ID) REFERENCES USER(USER_ID)
);