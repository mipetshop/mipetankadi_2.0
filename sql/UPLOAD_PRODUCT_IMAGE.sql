CREATE TABLE UPLOAD_PRODUCT_IMAGE(
   IMAGE_ID   INT  NOT NULL,
   IMAGE_NAME VARCHAR (100),
   CREATED_ON   DATETIME, 
   MODIFIED_ON   DATETIME, 
   CREATED_BY    INT,
   MODIFIED_BY INT,
   STATUS VARCHAR (10),
   PRODUCT_ID   INT,
   PRIMARY KEY (IMAGE_ID),
   FOREIGN KEY (PRODUCT_ID) REFERENCES UPLOAD_PRODUCT_DETAILS(PRODUCT_ID)
);