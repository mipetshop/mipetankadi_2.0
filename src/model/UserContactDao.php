<?php


class UserContactDao {
    public function getUserContact($contact_id, $user_id) {
        $user_sql = "SELECT * FROM USER_CONTACT WHERE CONTACT_ID CONTACT_ID = $contact_id AND USER_ID = $user_id";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $statement = $db_connection->query($user_sql);
            $users = $statement->fetchAll(PDO::FETCH_OBJ);
            $db_connection = null;
            echo json_encode($users);
        } catch (PDOException $ex) {
            echo '"{ERROR- UserDao->getUser": {",'.$ex->getMessage(). '}';
        }
    }
    /*
     * Add new user to USER_CONTACT table
     */         
    public function addUserContact($request) {
        $user_sql = "INSERT INTO USER_CONTACT(CONTACT_NAME, MOBILE_NO, ADDRESS_ONE, ADDRESS_TWO, CITY, "
                . "DISTRICT, STATE, COUNTRY, ZIP_CODE, CREATED_ON, MODIFIED_ON, CREATED_BY, MODIFIED_BY, STATUS, "
                . "IS_PRIMARY_CONTACT, USER_ID) VALUES (:CONTACT_NAME, :MOBILE_NO, :ADDRESS_ONE, "
                . ":ADDRESS_TWO, :CITY, :DISTRICT, :STATE, :COUNTRY, :ZIP_CODE, :CREATED_ON, :MODIFIED_ON, :CREATED_BY, "
                . ":MODIFIED_BY, :STATUS, :IS_PRIMARY_CONTACT, :USER_ID)";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $statement = $db_connection->prepare($user_sql);
            $statement->bindParam(':CONTACT_NAME',$request->getParam('CONTACT_NAME'));
            $statement->bindParam(':MOBILE_NO',$request->getParam('MOBILE_NO'));
            $statement->bindParam(':ADDRESS_ONE',$request->getParam('ADDRESS_ONE'));
            $statement->bindParam(':ADDRESS_TWO',$request->getParam('ADDRESS_TWO'));
            $statement->bindParam(':CITY',$request->getParam('CITY'));
            $statement->bindParam(':DISTRICT',$request->getParam('DISTRICT'));
            $statement->bindParam(':STATE',$request->getParam('STATE'));
            $statement->bindParam(':COUNTRY',$request->getParam('COUNTRY'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('ZIP_CODE'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('CREATED_ON'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('MODIFIED_ON'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('CREATED_BY'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('MODIFIED_BY'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('STATUS'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('IS_PRIMARY_CONTACT'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('USER_ID'));
            $statement->execute();
            $db_connection = null;
        } catch (PDOException $ex) {
            echo ''.$ex->getMessage();
        }
    }
    
    /*
     * Update USER_CONTACT details
     */
    public function updateUserContact($request, $contact_id, $user_id) {
        $user_sql = "UPDATE USER_CONTACT SET CONTACT_NAME=:CONTACT_NAME, MOBILE_NO=:MOBILE_NO, ADDRESS_ONE=:ADDRESS_ONE,"
                . " ADDRESS_TWO=:ADDRESS_TWO, CITY=:CITY, DISTRICT=:DISTRICT, STATE=:STATE, COUNTRY=:COUNTRY, "
                . "ZIP_CODE=:ZIP_CODE, CREATED_ON=:CREATED_ON, MODIFIED_ON=:MODIFIED_ON, CREATED_BY=:CREATED_BY, "
                . "MODIFIED_BY=:MODIFIED_BY, STATUS=:STATUS, IS_PRIMARY_CONTACT=:IS_PRIMARY_CONTACT, USER_ID=:USER_ID "
                . "WHERE CONTACT_ID = $contact_id AND USER_ID = $user_id";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $statement = $db_connection->prepare($user_sql);
            $statement->bindParam(':CONTACT_NAME',$request->getParam('CONTACT_NAME'));
            $statement->bindParam(':MOBILE_NO',$request->getParam('MOBILE_NO'));
            $statement->bindParam(':ADDRESS_ONE',$request->getParam('ADDRESS_ONE'));
            $statement->bindParam(':ADDRESS_TWO',$request->getParam('ADDRESS_TWO'));
            $statement->bindParam(':CITY',$request->getParam('CITY'));
            $statement->bindParam(':DISTRICT',$request->getParam('DISTRICT'));
            $statement->bindParam(':STATE',$request->getParam('STATE'));
            $statement->bindParam(':COUNTRY',$request->getParam('COUNTRY'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('ZIP_CODE'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('CREATED_ON'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('MODIFIED_ON'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('CREATED_BY'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('MODIFIED_BY'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('STATUS'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('IS_PRIMARY_CONTACT'));
            $statement->bindParam(':ZIP_CODE',$request->getParam('USER_ID'));
            $statement->execute();
            $db_connection = null;
        } catch (PDOException $ex) {
            echo '"{ERROR- UserDao->updateUser": {",'.$ex->getMessage(). '}';
        }
    }
}
