<?php


class UserDao{
    
    public function getUser($user_id) {
        $user_sql = "select * from user where USER_ID = $user_id";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $statement = $db_connection->query($user_sql);
            $users = $statement->fetchAll(PDO::FETCH_OBJ);
            $db_connection = null;
            echo json_encode($users);
        } catch (PDOException $ex) {
            echo '"{ERROR- UserDao->getUser": {",'.$ex->getMessage(). '}';
        }
    }
    /*
     * Add new user to USER table
     */         
    public function addUser($request) {
        $user_sql = "INSERT INTO USER(USER_NAME, USER_PASSWORD, DATE_OF_BIRTH, USER_IMAGE_NAME,"
                . "LAST_LOGIN, CREATED_ON, MODIFIED_ON, IS_ADMIN, STATUS) "
                . "VALUES(:USER_NAME, :USER_PASSWORD, :DATE_OF_BIRTH, :USER_IMAGE_NAME,"
                . ":LAST_LOGIN, :CREATED_ON, :MODIFIED_ON, :IS_ADMIN, :STATUS)";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $statement = $db_connection->prepare($user_sql);
            $statement->bindParam(':USER_NAME',$request->getParam('USER_NAME'));
            $statement->bindParam(':USER_PASSWORD',$request->getParam('USER_PASSWORD'));
            $statement->bindParam(':DATE_OF_BIRTH',$request->getParam('DATE_OF_BIRTH'));
            $statement->bindParam(':USER_IMAGE_NAME',$request->getParam('USER_IMAGE_NAME'));
            $statement->bindParam(':LAST_LOGIN',$request->getParam('LAST_LOGIN'));
            $statement->bindParam(':CREATED_ON',$request->getParam('CREATED_ON'));
            $statement->bindParam(':MODIFIED_ON',$request->getParam('MODIFIED_ON'));
            $statement->bindParam(':IS_ADMIN',$request->getParam('IS_ADMIN'));
            $statement->bindParam(':STATUS',$request->getParam('STATUS'));
            $statement->execute();
            $db_connection = null;
            echo '{"notice":{"text":"User added" "}"}';
        } catch (PDOException $ex) {
            echo ''.$ex->getMessage();
        }
    }
    
    /*
     * Update user details
     */
    public function updateUser($request, $user_id) {
        $user_sql = "UPDATE USER SET 
		USER_NAME = :USER_NAME, 
		USER_PASSWORD = :USER_PASSWORD, 
		DATE_OF_BIRTH = :DATE_OF_BIRTH, 
		USER_IMAGE_NAME = :USER_IMAGE_NAME,
                LAST_LOGIN=:LAST_LOGIN, 
		CREATED_ON=:CREATED_ON, 
		MODIFIED_ON=:MODIFIED_ON, 
		IS_ADMIN=:IS_ADMIN, 
		STATUS =:STATUS
			WHERE USER_ID = $user_id";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $statement = $db_connection->prepare($user_sql);
            $statement->bindParam(':USER_NAME',$request->getParam('USER_NAME'));
            $statement->bindParam(':USER_PASSWORD',$request->getParam('USER_PASSWORD'));
            $statement->bindParam(':DATE_OF_BIRTH',$request->getParam('DATE_OF_BIRTH'));
            $statement->bindParam(':USER_IMAGE_NAME',$request->getParam('USER_IMAGE_NAME'));
            $statement->bindParam(':LAST_LOGIN',$request->getParam('LAST_LOGIN'));
            $statement->bindParam(':CREATED_ON',$request->getParam('CREATED_ON'));
            $statement->bindParam(':MODIFIED_ON',$request->getParam('MODIFIED_ON'));
            $statement->bindParam(':IS_ADMIN',$request->getParam('IS_ADMIN'));
            $statement->bindParam(':STATUS',$request->getParam('STATUS'));
            $statement->execute();
            $db_connection = null;
            echo '{"notice":{"text":"User updated" "}"}';
        } catch (PDOException $ex) {
            echo '"{ERROR- UserDao->updateUser": {",'.$ex->getMessage(). '}';
        }
    }
}