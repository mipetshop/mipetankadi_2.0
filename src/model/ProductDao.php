<?php

class ProductDao{
    /**
     * Get all Product Categories which created by us and user.
     * @param type $category_id
     * @throws PDOException
     */
    public function getProdCategories($user_id) {
        $user_sql = "SELECT CATEGORY_ID, CATEGORY_NAME,CREATED_ON,MODIFIED_ON,"
                . "CREATED_BY,MODIFIED_BY,STATUS,USER_ID FROM PRODUCT_CATEGORY WHERE IS_USER_CREATED IS NULL UNION"
                . " (SELECT CATEGORY_ID, CATEGORY_NAME,CREATED_ON,MODIFIED_ON, "
                . "CREATED_BY,MODIFIED_BY,STATUS,USER_ID FROM PRODUCT_CATEGORY WHERE IS_USER_CREATED = 'Y' AND USER_ID = $user_id) ";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $statement = $db_connection->query($user_sql);
            $users = $statement->fetchAll(PDO::FETCH_OBJ);
            $db_connection = null;
            echo json_encode($users);
        } catch (PDOException $ex) {
            echo ''.$ex->getMessage();
        }
    }
    
     /**
     * Get all Sub Product Categories which created by us and user.
     * @param type $category_id
     * @throws PDOException
     */
    public function getSubProdCategories($user_id, $category_id) {
        $user_sql = "SELECT SUB_CATEGORY_ID,SUB_CATEGORY_NAME, CREATED_ON, MODIFIED_ON, IS_USER_CREATED, CREATED_BY, "
                . "MODIFIED_BY, STATUS, USER_ID, CATEGORY_ID FROM SUB_PRODUCT_CATEGORY WHERE IS_USER_CREATED IS NULL AND CATEGORY_ID = $category_id UNION "
                . " (SELECT SUB_CATEGORY_ID,SUB_CATEGORY_NAME, CREATED_ON, MODIFIED_ON, IS_USER_CREATED, CREATED_BY, "
                . "MODIFIED_BY, STATUS, USER_ID, CATEGORY_ID FROM SUB_PRODUCT_CATEGORY WHERE IS_USER_CREATED = 'Y' AND "
                . "USER_ID = $user_id AND CATEGORY_ID = $category_id) ";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $statement = $db_connection->query($user_sql);
            $users = $statement->fetchAll(PDO::FETCH_OBJ);
            $db_connection = null;
            echo json_encode($users);
        } catch (PDOException $ex) {
            echo ''.$ex->getMessage();
        }
    }
    /**
     * Add new product category
     * @param type $request
     * @throws PDOException
     */
    public function addProdCategory($request) {
        $user_sql = "INSERT INTO PRODUCT_CATEGORY(CATEGORY_NAME,CREATED_ON,MODIFIED_ON,"
                . "CREATED_BY,MODIFIED_BY,STATUS,USER_ID, IS_USER_CREATED) VALUES(:CATEGORY_NAME,:CREATED_ON,"
                . ":MODIFIED_ON,:CREATED_BY,:MODIFIED_BY,:STATUS,:USER_ID,:IS_USER_CREATED)";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $statement = $db_connection->prepare($user_sql);
            $statement->bindParam(':CATEGORY_NAME',$request->getParam('CATEGORY_NAME'));
            $statement->bindParam(':CREATED_ON',$request->getParam('CREATED_ON'));
            $statement->bindParam(':MODIFIED_ON',$request->getParam('MODIFIED_ON'));
            $statement->bindParam(':CREATED_BY',$request->getParam('CREATED_BY'));
            $statement->bindParam(':MODIFIED_BY',$request->getParam('MODIFIED_BY'));
            $statement->bindParam(':STATUS',$request->getParam('STATUS'));
            $statement->bindParam(':USER_ID',$request->getParam('USER_ID'));
            $statement->bindParam(':IS_USER_CREATED',$request->getParam('IS_USER_CREATED'));
            $statement->execute();
            $db_connection = null;
            echo '{"notice":{"text":"New Product category added added" "}"}';
        } catch (PDOException $ex) {
            echo ''.$ex->getMessage();
        }
    }
    
    /**
     * Add new sub product Categories
     * @param type $request
     * @throws PDOException
     */
    public function addSubProdCategory($request) {
        $user_sql = "INSERT INTO sub_product_category( SUB_CATEGORY_NAME, CREATED_ON, MODIFIED_ON, IS_USER_CREATED, "
                . "CREATED_BY, MODIFIED_BY, STATUS, USER_ID, CATEGORY_ID) VALUES (:SUB_CATEGORY_NAME,:CREATED_ON,"
                . ":MODIFIED_ON,:IS_USER_CREATED,:CREATED_BY,:MODIFIED_BY,:STATUS,:USER_ID,:CATEGORY_ID)";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $statement = $db_connection->prepare($user_sql);
            $statement->bindParam(':SUB_CATEGORY_NAME',$request->getParam('SUB_CATEGORY_NAME'));
            $statement->bindParam(':CREATED_ON',$request->getParam('CREATED_ON'));
            $statement->bindParam(':MODIFIED_ON',$request->getParam('MODIFIED_ON'));
            $statement->bindParam(':CREATED_BY',$request->getParam('CREATED_BY'));
            $statement->bindParam(':MODIFIED_BY',$request->getParam('MODIFIED_BY'));
            $statement->bindParam(':STATUS',$request->getParam('STATUS'));
            $statement->bindParam(':USER_ID',$request->getParam('USER_ID'));
            $statement->bindParam(':IS_USER_CREATED',$request->getParam('IS_USER_CREATED'));
            $statement->bindParam(':CATEGORY_ID',$request->getParam('CATEGORY_ID'));
            $statement->execute();
            $db_connection = null;
            echo '{"notice":{"text":"New Product category added added" "}"}';
        } catch (PDOException $ex) {
            echo ''.$ex->getMessage();
        }
    }
    
    public function updateProdCategory($request, $category_id) {
        $user_sql = "UPDATE PRODUCT_CATEGORY SET "
                . "CATEGORY_NAME=:CATEGORY_NAME,CREATED_ON=:CREATED_ON,MODIFIED_ON=:MODIFIED_ON,"
                . "CREATED_BY=:CREATED_BY,MODIFIED_BY=:MODIFIED_BY,STATUS=:STATUS,USER_ID=:USER_ID,IS_USER_CREATED=:IS_USER_CREATED "
                . "WHERE CATEGORY_ID=$category_id";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $statement = $db_connection->prepare($user_sql);
            $statement->bindParam(':CATEGORY_NAME',$request->getParam('CATEGORY_NAME'));
            $statement->bindParam(':CREATED_ON',$request->getParam('CREATED_ON'));
            $statement->bindParam(':MODIFIED_ON',$request->getParam('MODIFIED_ON'));
            $statement->bindParam(':CREATED_BY',$request->getParam('CREATED_BY'));
            $statement->bindParam(':MODIFIED_BY',$request->getParam('MODIFIED_BY'));
            $statement->bindParam(':STATUS',$request->getParam('STATUS'));
            $statement->bindParam(':USER_ID',$request->getParam('USER_ID'));
            $statement->bindParam(':IS_USER_CREATED',$request->getParam('IS_USER_CREATED'));
            $statement->execute();
            $db_connection = null;
            echo '{"notice":{"text":"Product category updated" "}"}';
        } catch (PDOException $ex) {
            echo 'Ex --> '.$ex->getMessage();
        }
    }
    
    public function updateSubProdCategory($request,$category_id,$sub_category_id) {
        $user_sql = "UPDATE sub_product_category SET SUB_CATEGORY_NAME=:SUB_CATEGORY_NAME,CREATED_ON=:CREATED_ON,"
                . "MODIFIED_ON=:MODIFIED_ON,IS_USER_CREATED=:IS_USER_CREATED,CREATED_BY=:CREATED_BY,MODIFIED_BY=:MODIFIED_BY,"
                . "STATUS=:STATUS,USER_ID=:USER_ID,CATEGORY_ID=:CATEGORY_ID WHERE SUB_CATEGORY_ID = $sub_category_id "
                . "and CATEGORY_ID = $category_id";
        try {
            $db_connection = new DbConnection();
            $db_connection = $db_connection->connect();
            $sub_product = json_decode($request->getBody());
            $statement = $db_connection->prepare($user_sql);
            $statement->bindParam(':SUB_CATEGORY_NAME',$sub_product->SUB_CATEGORY_NAME);
            $statement->bindParam(':CREATED_ON',$sub_product->CREATED_ON);
            $statement->bindParam(':MODIFIED_ON',$sub_product->MODIFIED_ON);
            $statement->bindParam(':CREATED_BY',$sub_product->CREATED_BY);
            $statement->bindParam(':MODIFIED_BY',$sub_product->MODIFIED_BY);
            $statement->bindParam(':STATUS',$sub_product->STATUS);
            $statement->bindParam(':USER_ID',$sub_product->USER_ID);
            $statement->bindParam(':IS_USER_CREATED',$sub_product->IS_USER_CREATED);
            $statement->bindParam(':CATEGORY_ID',$sub_product->CATEGORY_ID);
            $statement->execute();
            $db_connection = null;
            echo '{"notice":{"text":"Product category updated" "}"}';
        } catch (PDOException $ex) {
            echo 'Ex --> '.$ex->getMessage();
        }
    }
}