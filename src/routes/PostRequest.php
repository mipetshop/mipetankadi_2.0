<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


$app->post('/users', function (Request $request, Response $response) {
    $user_dao = new UserDao();
    try {
        $user_dao->addUser($request);
    } catch (Exception $exc) {
        echo json_encode($exc);
    }
    return $response;
});

$app->post('/product/category', function (Request $request, Response $response) {
    $product_dao = new ProductDao();
    try {
        $product_dao->addProdCategory($request);
    } catch (Exception $exc) {
        echo json_encode($exc);
    }
    return $response;
});

$app->post('/product/subcategory', function (Request $request, Response $response) {
    $product_dao = new ProductDao();
    try {
        $product_dao->addSubProdCategory($request);
    } catch (Exception $exc) {
        echo json_encode($exc);
    }
    return $response;
});