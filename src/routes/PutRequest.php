<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->put('/users/{id}', function (Request $request, Response $response) {
    $user_dao = new UserDao();
    $user_id = $request->getAttribute('id');
    $user_dao->updateUser($request,$user_id);
    return $response;
});

$app->put('/product/category/{category_id}', function (Request $request, Response $response) {
    $product_dao = new ProductDao();
    $category_id = $request->getAttribute('category_id');
    $product_dao->updateProdCategory($request,$category_id);
    return $response;
});

$app->put('/product/subcategory/{category_id}/{sub_category_id}', function (Request $request, Response $response) {
    $product_dao = new ProductDao();
    $category_id = $request->getAttribute('category_id');
    $sub_category_id = $request->getAttribute('sub_category_id');
    $product_dao->updateSubProdCategory($request,$category_id,$sub_category_id);
    return $response;
});
