<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
require '../src/model/UserDao.php';
require '../src/model/ProductDao.php';

$app->get('/users/{user_id}', function (Request $request, Response $response) {
    $user_dao = new UserDao();
    $user_id  = $request->getAttribute('user_id'); 
    $user_dao->getUser($user_id);
    return $response;
});

$app->get('/product/categories/{user_id}', function (Request $request, Response $response) {
    $product_dao = new ProductDao();
    $user_id  = $request->getAttribute('user_id'); 
    $product_dao->getProdCategories($user_id);
    return $response;
});

$app->get('/product/subcategories/{user_id}/{category_id}', function (Request $request, Response $response) {
    $product_dao = new ProductDao();
    $user_id  = $request->getAttribute('user_id'); 
    $category_id  = $request->getAttribute('category_id');
    $product_dao->getSubProdCategories($user_id,$category_id);
    return $response;
});