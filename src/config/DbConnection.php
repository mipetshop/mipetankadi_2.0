<?php

class DbConnection{
    // Properties
    private $dbhost = 'localhost';
    private $dbuser = 'root';
    private $dbpass = ''; // Nothing we set
    private $dbname = 'petankadi';
    
    public function connect(){
        $db_connection = new PDO("mysql:host=$this->dbhost;dbname=$this->dbname", $this->dbuser, $this->dbpass);  
        $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db_connection;
    }
}

